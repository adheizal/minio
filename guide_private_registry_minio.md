##### Private Registry with MinIO (Cluster)

Detail VM :
- MinIO-01
  CPU : 2 core
  RAM : 4 GB
  IP : 192.168.100.102
  DISK : 
  - vdb : 10 GB
  - vdc : 10 GB
  - vdd : 10 GB
- MinIO-02
  CPU : 2 core
  RAM : 4 GB
  IP : 192.168.100.101
  DISK : 
  - vdb : 10 GB
  - vdc : 10 GB
  - vdd : 10 GB
- MinIO-03
  CPU : 2 core
  RAM : 4 GB
  IP : 192.168.100.103
  DISK : 
  - vdb : 10 GB
  - vdc : 10 GB
  - vdd : 10 GB

## Create Cluster MinIO
#### exec in each VM MinIO

1. Format Disk For MinIO
```bash
$ sudo parted -s -a optimal -- /dev/vdx mklabel gpt
$ sudo parted -s -a optimal -- /dev/vdx mkpart primary 0% 100%
$ sudo parted -s -- /dev/vdx align-check optimal 1
$ sudo mkfs.ext4 /dev/vdx1
```
2. Mount Disk to Data
```bash
$ echo "/dev/vdx1 /data-01 ext4 defaults 0 0" | sudo tee -a /etc/fstab
$ sudo mkdir /data-01
$ sudo mount -a
```
3. Intall MinIO
```bash
$ wget https://dl.minio.io/server/minio/release/linux-amd64/minio
$ chmod +x minio
$ sudo mv minio /usr/local/bin
```
4. Run MinIO in each VM MinIO
```bash
$ screen -R minio
$ sudo -i
# export MINIO_ACCESS_KEY=7SBGPQB4C46HJADDJJ93
# export MINIO_SECRET_KEY=FeQ1mSL3i++9O+rAayRF6WWjfLGqE+LSIDn7waV3
# minio server --address :9000 http://192.168.100.102/data-01 http://192.168.100.102/data-02 http://192.168.100.102/data-03 http://192.168.100.101/data-01 http://192.168.100.101/data-02 http://192.168.100.101/data-03 http://192.168.100.103/data-01 http://192.168.100.103/data-02 http://192.168.100.103/data-03
```
you will get output like this
```bash
Status:         9 Online, 0 Offline.
Endpoint:  http://192.168.100.101:9000  http://127.0.0.1:9000    
AccessKey: 7SBGPQB4C46HJADDJJ93 
SecretKey: FeQ1mSL3i++9O+rAayRF6WWjfLGqE+LSIDn7waV3 

Browser Access:
   http://192.168.100.102:9000  http://127.0.0.1:9000

Command-line Access: https://docs.min.io/docs/minio-client-quickstart-guide
   $ mc alias set myminio http://192.168.100.101:9000 7SBGPQB4C46HJADDJJ93 FeQ1mSL3i++9O+rAayRF6WWjfLGqE+LSIDn7waV3

Object API (Amazon S3 compatible):
   Go:         https://docs.min.io/docs/golang-client-quickstart-guide
   Java:       https://docs.min.io/docs/java-client-quickstart-guide
   Python:     https://docs.min.io/docs/python-client-quickstart-guide
   JavaScript: https://docs.min.io/docs/javascript-client-quickstart-guide
   .NET:       https://docs.min.io/docs/dotnet-client-quickstart-guide
```

5. Check Dashboard MinIO
* access browser http://103.89.7.156:9000
* Create Bucket docker-registry

#### exec in one node
6. Setup Registry
- Create Password for registry
```
htpasswd -Bbn admin Rahasia > htpasswd
mkdir docker_auth
cp htpasswd docker_auth
```

- Create Custome Image Registry
```bash
$ mkdir minio && cd minio
$ vim Dockerfile
```
```bash
FROM registry:2
COPY config.yml /etc/docker/registry/config.yml
```
```bash
$ vim config.yml
...
version: 0.1
log:
    fields:
        service: registry
http:
    addr: :5000
storage:
    cache:
        layerinfo: inmemory
    s3:
        accesskey: 7SBGPQB4C46HJADDJJ93
        secretkey: FeQ1mSL3i++9O+rAayRF6WWjfLGqE+LSIDn7waV3
        region: us-east-1
        regionendpoint: https://minio.goreng.cc
        bucket: docker-registry
        encrypt: false
        secure: true
        v4auth: true
        chunksize: 5242880
...

$ docker build -t minio .
```

- Create Docker-compose for Run Registry
```bash
$ cd
$ vim docker-compose.yml 
...
version: '3'
services:
  registry:
    image: minio:latest 
    restart: always
    ports:
      - "5000:5000"
    environment:
      - REGISTRY_AUTH=htpasswd
      - REGISTRY_AUTH_HTPASSWD_REALM="Registry Realm"
      - REGISTRY_AUTH_HTPASSWD_PATH=/auth/htpasswd
    volumes:
      - ./docker_auth:/auth 
      - ./data/docker-registry
...

$ docker-compose up -d
```


##### Private Registry with MinIO (Standalone)

Detail VM :  
- MinIO-demo-01  
  CPU : 2 core  
  RAM : 4 GB  
  IP : 192.168.100.6  
  DISK :   
  - vdb : 10 GB

## Create Cluster MinIO
#### exec in VM MinIO

1. Format Disk For MinIO
```bash
$ sudo parted -s -a optimal -- /dev/vdb mklabel gpt
$ sudo parted -s -a optimal -- /dev/vdb mkpart primary 0% 100%
$ sudo parted -s -- /dev/vdb align-check optimal 1
$ sudo mkfs.ext4 /dev/vdb1
```
2. Mount Disk to Data
```bash
$ echo "/dev/vdb1 /data-01 ext4 defaults 0 0" | sudo tee -a /etc/fstab
$ sudo mkdir /data-01
$ sudo mount -a
```
3. Intall MinIO
```bash
$ wget https://dl.minio.io/server/minio/release/linux-amd64/minio
$ chmod +x minio
$ sudo mv minio /usr/local/bin
```
4. Create user MinIO
```bash
$ sudo useradd --system minio-user --shell /sbin/nologin
```

5. create directories and chown it to minio-use
```bash
$ sudo mkdir -p /data/
$ sudo mkdir /etc/minio
$ sudo chown minio-user:minio-user /data/
$ sudo chown minio-user:minio-user /etc/minio
```

6. Create default config MinIO
```bash
$ sudo vi /etc/default/minio
...
# Volume to be used for Minio server.
MINIO_VOLUMES="/data"
# Use if you want to run Minio on a custom port.
MINIO_OPTS="--address :9000"
# Access Key of the server.
MINIO_ACCESS_KEY=BKIKJAA5BMMU2RHO6IBB
# Secret key of the server.
MINIO_SECRET_KEY=V7f1CwQqAcwo80UEIJEjc5gVQUSSx5ohQ9GSrr12
...
```

7. Run MinIO systemd
```bash
$ curl -O https://raw.githubusercontent.com/minio/minio-service/master/linux-systemd/minio.service
$ sudo mv minio.service /etc/systemd/system
$ sudo systemctl daemon-reload
$ sudo systemctl enable minio
$ sudo systemctl start minio
$ sudo systemctl status minio -l
```

8. Check Dashboard MinIO
* access browser http://103.89.0.184:9000

Login use Access Key and Secret Key
``` bash
$ cat /data/.minio.sys/config/config.json | grep accessKey
$ cat /data/.minio.sys/config/config.json | grep secretKey
```

##### Create Bucket docker-registry

9. Setup Registry
- Create Password for registry
```bash
$ htpasswd -Bbn admin Rahasia > htpasswd
$ mkdir docker_auth
$ cp htpasswd docker_auth
```

- Create Custome Image Registry
```bash
$ mkdir minio && cd minio
$ vim Dockerfile
...
FROM registry:2
COPY config.yml /etc/docker/registry/config.yml
...

$ vim config.yml
...
version: 0.1
log:
    fields:
        service: registry
http:
    addr: :5000
storage:
    cache:
        layerinfo: inmemory
    s3:
        accesskey: 7SBGPQB4C46HJADDJJ93
        secretkey: FeQ1mSL3i++9O+rAayRF6WWjfLGqE+LSIDn7waV3
        region: us-east-1
        regionendpoint: https://minio.goreng.cc
        bucket: docker-registry
        encrypt: false
        secure: true
        v4auth: true
        chunksize: 5242880
...

$ docker build -t minio .
```
- Create Docker-compose for Run Registry
```bash
$ cd
$ vim docker-compose.yml 
...
version: '3'
services:
  registry:
    image: minio:latest 
    restart: always
    ports:
      - "5000:5000"
    environment:
      - REGISTRY_AUTH=htpasswd
      - REGISTRY_AUTH_HTPASSWD_REALM="Registry Realm"
      - REGISTRY_AUTH_HTPASSWD_PATH=/auth/htpasswd
    volumes:
      - ./docker_auth:/auth 
      - ./data/docker-registry
...
$ docker-compose up -d
```